# HelloWorld
This is a simple sample on how you can query, move and rotate entities. This should be the first sample to explore when first learning Tiny. 

### Date and version
v1, 5/31/2019

### Related API documentation
* [EntityQueryBuilding.WithAll](https://docs.unity3d.com/Packages/com.unity.entities@0.0/api/Unity.Entities.EntityQueryBuilder.html#Unity_Entities_EntityQueryBuilder_WithAll__1)
* [EntityQueryBuilding.ForEach](https://docs.unity3d.com/Packages/com.unity.entities@0.0/api/Unity.Entities.EntityQueryBuilder.html#Unity_Entities_EntityQueryBuilder_ForEach_Unity_Entities_EntityQueryBuilder_F_E_)
* [World.GetExistingSystem](https://docs.unity3d.com/Packages/com.unity.entities@0.0/api/Unity.Entities.World.html#Unity_Entities_World_GetExistingSystem_System_Type_)

### Prerequisite samples	
none

### Samples extending this sample
* [DragAndDrop](../DragAndDrop)
* [Joystick](../Joystick)

### File Descriptions 
* [RotateSystem](Scripts/RotateSystem.cs): Animate the rotation of any entity that has the [Rotate](Components/Rotate.cs) component.

### Resources
Font: [The Bold Font](https://www.dafont.com/the-bold-font.font)

### Feedback
We are always looking to understand how we can improve our samples. If you are able, we would love to get your feedback: https://unitysoftware.co1.qualtrics.com/jfe/form/SV_3BOKADHkJsMdJrL
